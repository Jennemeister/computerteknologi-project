#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry        
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import Twist                                                 
from sensor_msgs.msg import LaserScan
import math

roll = pitch = yaw = 0.0 #globally sets yawpitch and roll to 0 yaw is the important one

kP = 0.4        #this is a constant which is used so that the angular velocity doesn't get too high
var_v = 0.001   # var_v and var_w are two difference variable for uncertaincies in calculations
var_w = 0.01    # Did we not use these, the robot would act wierdly in some situations


# Goes forward if front is clear
# otherwises call cwall()
def callback(msg):
    if msg.ranges[0] > 0.8:
        command.linear.x = 0.2      #drive forward at a velocity of 0.2
        command.angular.z = 0.0     #do not rotate
    else:
        cwall(msg)                  #goes to function Cwall
    pub.publish(command)            #publishes what is in command


# Get the yaw of the turtlebot
# to understand how it's facing
def get_rotation (msg):
    global roll, pitch, yaw
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, yaw) = euler_from_quaternion (orientation_list)


# Get a degree and return
# radian output for rotation
def rotation(d):
    d = (d*math.pi/180)
    command.angular.z = -kP * (d - yaw)     #sets the rotation velocity and which way it rotates 
    command.linear.x = 0.0                  #don't drive
    return d                                #returns the radian of angle d


def cwall(msg):
    if(msg.ranges[180] <= 0.3): #checks if there are any objects near the back of the turtlebot if there are then it should drive forward
        command.linear.x = 0.1      #go forwards
        command.angular.z = 0       #don't rotate
    elif(msg.ranges[90] <= msg.ranges[270]+0.2 and msg.ranges[90] >= msg.ranges[270]-0.2): #checks if the turtlebot has walls on both sides (this is to make it more decisive when near a wall)
        command.angular.z = 0.1     #rotate counter clock wise
        command.linear.x =-0.2      #go backwards
        pub.publish(command)        #publish command
    elif(msg.ranges[90] < 0.8 and msg.ranges[90] < msg.ranges[270]): #checks if there is a wall which is close to the turtlebot on its left side                   
        if yaw <= var_v and yaw >= -var_v:          # We have 4 cases the first when our angular position in radians (yaw) is close to 0 then it should turn the other way
            target_rad = rotation(-90)              # call rotation with a degree and turn into radians
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)):  #while yaw isn't near target then it should keep rotating 
                pub.publish(command)                                                    #publishes what is currently on command
        elif yaw <= (math.pi/2)+var_v and yaw >= (math.pi/2)-var_v:     #second case when our angular position (yaw) is close to 90 degrees then:
            target_rad = rotation(0)                               
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
        elif (yaw <= (rotation(179.5))+var_v and yaw >= (rotation(179.5))-var_v ) or (yaw <= (rotation(-179.5))+var_v and yaw >= (rotation(-179.5))-var_v ): #3rd case if yaw is near 180 degrees then:
            target_rad = rotation(90)
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
        elif yaw <= -(math.pi/2)+var_v and yaw >= -(math.pi/2)-var_v:       #if yaw is near 270 degrees then:
            target_rad = rotation(179.5)
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
    elif(msg.ranges[270] < 0.8 and msg.ranges[270] < msg.ranges[90]): # here we do the same as above but instead of checking the wall on the lef side of the turtle bot then it checks on the right
        if yaw <= var_v and yaw >= -var_v:      #case 1 if yaw is near 0 then rotate to the left:
            target_rad = rotation(90)  
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
        elif yaw <= (math.pi/2)+var_v and yaw >= (math.pi/2)-var_v: #case 2 if yaw is near 90 then:
            target_rad = rotation(179.5)
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
        elif (yaw <= (rotation(179.5))+var_v and yaw >= (rotation(179.5))-var_v ) or (yaw <= (rotation(-179.5))+var_v and yaw >= (rotation(-179.5))-var_v):  #case 3 if yaw is near 180 then:
            target_rad = rotation(-90)
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
        elif yaw <= -(math.pi/2)+var_v and yaw >= -(math.pi/2)-var_v:    #case 4 if yaw is near 270 then:
            target_rad = rotation(0)
            while((yaw <= target_rad+var_w and yaw >= target_rad-var_w) or (yaw >= target_rad+var_w and yaw <= target_rad-var_w)): 
                pub.publish(command)
    elif(msg.ranges[0] < 0.8 ):         #checks if the turtlebot is near a wall directly in front of it
        command.angular.z = 0.1         #makes it rotate counter clock wise
        command.linear.x = -0.2         #makes it drive backwards
        pub.publish(command)
           

rospy.init_node('rotate_robot') #initiate node "rotate_robot" 
subl = rospy.Subscriber('scan', LaserScan, callback)     # Subscribe to callback
sub = rospy.Subscriber ('/odom', Odometry, get_rotation) # Subscribe to get_rotation
pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)   # Publish to twist
command = Twist()
r = rospy.Rate(10)  # Set the object rate to 10Hz
pub.publish(command)
r.sleep()

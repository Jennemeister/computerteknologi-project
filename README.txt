# Computerteknologi Project

Authors: Jens Dissing (201909100) and Henrik Buhl (201905590)

Prerequisits:
Working computer with Ubuntu 18.4 LTS.
ROS packages installed.
Gazebo installed.

Known issues:
Setting parameters bashrc excatly as seen in the ROS tutorials.
Failing to source bashrc will create some problems. Remember to do this.

In a terminal:
$ cd catkin_ws/src/turtlebot3
$ catkin_create_pkg rotate_robot rospy
$ cd ~/catkin_ws/src/turtlebot3/rotate_robot
$ mkdir <your_directory_name>
$ cd <your_directory_name>
$ git clone https://gitlab.com/Jennemeister/computerteknologi-project.git
$ cd ~/catkin_ws
$ catkin_make
In 1 terminal run:
$ roslaunch turtlebot3_gazebo turtlebot3_world.launch
In another terminal run:
$ cd path/to/your/directory/here
$ python main.py

To run other maps, in a terminal run
$ roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
